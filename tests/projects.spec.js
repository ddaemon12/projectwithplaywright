const {test, expect} = require('@playwright/test')
const {Headerpage} = require('../page/header.page.js')

test('Selenium projects are present on the Projects-page', async ({page}) =>{
    const projPage = new Headerpage(page)
    await projPage.goto()
    await projPage.projects()
    await expect(projPage.projectsList).toHaveText([
        'Selenium WebDriver',
        'Selenium IDE',
        'Selenium Grid'
    ])
})