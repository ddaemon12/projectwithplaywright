const {test, expect} = require('@playwright/test')
const {Headerpage} = require('../page/header.page')

test('The "Downloads" button of the header redirects to correct page', async ({page}) =>{
    const downPage = new Headerpage(page)
    await downPage.goto()
    await downPage.downloads()

    await expect(page).toHaveURL('https://www.selenium.dev/downloads/')
})

test('The "Documentation" button of the header redirects to correct page', async ({page}) =>{
    const downPage = new Headerpage(page)
    await downPage.goto()
    await downPage.documentation()

    await expect(page).toHaveURL('https://www.selenium.dev/documentation/')
})

test('The "Projects" button of the header redirects to correct page', async ({page}) =>{
    const downPage = new Headerpage(page)
    await downPage.goto()
    await downPage.projects()

    await expect(page).toHaveURL('https://www.selenium.dev/projects/')
})

test('The "Support" button of the header redirects to correct page', async ({page}) =>{
    const downPage = new Headerpage(page)
    await downPage.goto()
    await downPage.support()

    await expect(page).toHaveURL('https://www.selenium.dev/support/')
})

test('The "Blog" button of the header redirects to correct page', async ({page}) =>{
    const downPage = new Headerpage(page)
    await downPage.goto()
    await downPage.blog()

    await expect(page).toHaveURL('https://www.selenium.dev/blog/')
})