const {test, expect} = require('@playwright/test')
const {Headerpage} = require('../page/header.page.js')

test('Support-page includes ways to help customer', async ({page}) =>{
    const supPage = new Headerpage(page)
    await supPage.goto()
    await supPage.support()
    await expect(supPage.helpWays).toHaveText([
        'User Group',
        'Chat Room',
        'Bug Tracker',
        'Commercial Support',
        'Certification and Training'
    ])
})