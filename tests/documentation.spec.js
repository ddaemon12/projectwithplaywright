const {test, expect} = require('@playwright/test')
const {Headerpage} = require('../page/header.page.js')


test('Documentation-page includes side-navbar with items', async ({page}) =>{
    const docPage = new Headerpage(page)
    await docPage.goto()
    await docPage.documentation()
    await expect(docPage.locNavBar).toHaveText([
        'Overview',
        'WebDriver',
        'Grid',
        'IE Driver Server',
        'IDE',
        'Test Practices',
        'Legacy',
        'About'
    ])
})
