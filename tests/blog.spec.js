const {test, expect} = require('@playwright/test')
const {Headerpage} = require('../page/header.page.js')

test('Blog-page includes navbar with particular units in it', async ({page}) =>{
    const blogPage = new Headerpage(page)
    await blogPage.goto()
    await blogPage.blog()
    await expect(blogPage.blogNavBar).toHaveText([
        '2022', // <= bug here
        '2021',
        '2020',
        '2019',
        '2018',
        '2017',
        '2016',
        '2015',
        '2014',
        '2013',
        '2012',
        '2011',
        '2010'
    ])
})