const {expect} = require('@playwright/test')

exports.Headerpage = class Headerpage{

    constructor(page){
        this.page = page;
        // this.about = page.locator('text=About')
        this.downloadsLink = page.locator('text=Downloads').first()
        this.documentationLink = page.locator('text=Documentation').first()
        this.projectsLink = page.locator('text=Projects').first()
        this.supportLink = page.locator('text=Support').first()
        this.blogLink = page.locator('text=Blog').first()
        
        this.locNavBar = page.locator('aside > div > nav > ul > li > ul > li > a > span')
        this.projectsList = page.locator('main > div > div > h2')
        this.helpWays = page.locator('main > div > div > h2')
        this.blogNavBar = page.locator('aside > div > nav > ul > li > ul > li > a > span')
    }

    async goto(){
        await this.page.goto('https://www.selenium.dev/downloads/')
    }

    async downloads(){
        await this.downloadsLink.click()
    }

    async documentation(){
        await this.documentationLink.click()
        
    }

    async projects(){
        await this.projectsLink.click()
    }

    async support(){
        await this.supportLink.click()
    }

    async blog(){
        await this.blogLink.click()
    }
}